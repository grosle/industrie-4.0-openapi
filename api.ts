import { api, body, endpoint, pathParams, request, response } from "@airtasker/spot";

@api({ name: "rest-api", version: "0.0.1" })
class Api {}

@endpoint({
  method: "POST",
  path: "/orders"
})
class PostOrders {
  @request
  request(
    @body body: Order
  ) {}

  @response({ status: 201 })
  successfulResponse(
    @body body: Order
  ) {}
}

@endpoint({
  method: "GET",
  path: "/orders"
})
class GetOrders {
  @request
  request() {}

  @response({status: 200})
  successfulResponse(@body body: Order[]) {}
}

@endpoint({
  method: "GET",
  path: "/orders/:id"
})
class GetOrder {
  @request
  request(@pathParams pathParams: {id: number}) {}

  @response({status: 200})
  response(@body body: Order) {}
}

@endpoint({
  method: 'GET',
  path: '/items'
})
class GetItems {
  @request request() {}

  @response({status: 200}) response(@body body: Item[]) {}
}

type OrderStatus = 'ACCEPTED' | 'IN_PROGRESS' | 'DONE'

interface Order {
  id?: number
  totalPrice?: number
  items?: Item[]
  status?: OrderStatus
}

interface Item {
  id?: number
  name?: string
  amount?: number
  price?: number
}
